import { Page, expect } from '@playwright/test'

export default class HomePage {
    baseURL
    page

    // Navigation
    home
    signUp
    profiles
    tweetButton
    modal
    makeNewTweetButton
    submitNewTweetButton
    tweetTextbox

    constructor(page: Page) {
        this.page = page
        this.baseURL = 'http://localhost:3000/'
        this.home = this.page.locator('[href="/"]')
        this.signUp = this.page.locator('[data-cy="nav-signup-link"]')
        this.profiles = this.page.locator('[href="/profiles"]')
        this.tweetButton = this.page.getByRole('button', { name: 'Tweet' })
        this.modal = this.page.locator('[data-reach-dialog-overlay]')
        this.tweetTextbox = this.page.locator('[data-reach-dialog-overlay] textarea')
        this.makeNewTweetButton = () => {
            return this.modal.getByRole('button', { name: 'Tweet' })
        }
        this.page.getByRole('button', { name: 'Tweet' })
        this.submitNewTweetButton = this.page.getByRole('button', { name: 'Tweet' })
    }

    async navigateToHomePage() {
        await this.page.goto(this.baseURL)
    }

    async verifyPageLoad() {
        await this.navigateToHomePage()
        await expect(this.home).toBeVisible()
        await expect(this.signUp).toBeVisible()
        await expect(this.profiles).toBeVisible()
    }

    async navigateToSignUp() {
        await this.signUp.click()
    }

    async verifyUserIsLoggedIn(shouldBeLoggedIn = true) {
        shouldBeLoggedIn
          ? await expect(this.tweetButton).toBeVisible
          : await expect(this.tweetButton).toBeHidden
    }

    async makeNewTweet(text: string) {
        await this.tweetButton.click()
        await this.tweetTextbox.fill(text)
        await this.submitNewTweetButton.click()
    }
}