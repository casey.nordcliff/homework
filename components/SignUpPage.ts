import { Page, expect } from '@playwright/test'

export default class SignUpPage {
    page

    // Components
    name
    userName
    email
    password
    confirmPassword
    submitButton

    constructor(page: Page) {
        this.page = page

        // Components
        this.name = this.page.locator('[data-cy="signup-name-input"]')
        this.userName = this.page.locator('[data-cy="signup-username-input"]')
        this.email = this.page.locator('[data-cy="signup-email-input"]')
        this.password = this.page.locator('[data-cy="signup-password-input"]')
        this.confirmPassword = this.page.locator('[data-cy="signup-password2-input"]')
        this.submitButton = this.page.locator('[data-cy="signup-submit"]')
    }

    generateDefaultInfo() {
        const now = Date.now()
        const name = `Casey-${now}`
        const userName = `casey.${now}`
        const email = `casey.${now}@gmail.com`
        const password = 'CarpetTelevisionCanineSleeping1@'
        const confirmPassword = password
        return { name, userName, email, password, confirmPassword }
    }

    async enterName(name: string) {
        await this.name.fill(name)
    }

    async enterUserName(userName: string) {
        await this.userName.fill(userName)
    }

    async enterEmail(email: string) {
        await this.email.fill(email)
    }

    async enterPassword(password: string) {
        await this.password.fill(password)
    }

    async enterConfirmPassword(password: string) {
        await this.confirmPassword.fill(password)
    }

    async submit() {
        await this.submitButton.click()
    }

    async signUpNewUser() {
        const { name, userName, email, password, confirmPassword } = this.generateDefaultInfo()
        await this.enterName(name)
        await this.enterUserName(userName)
        await this.enterEmail(email)
        await this.enterPassword(password)
        await this.enterConfirmPassword(confirmPassword)
        await this.submit()
        return email
    }
}