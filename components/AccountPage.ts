

import { Page } from '@playwright/test'

export default class AccountPage {
    page

    // Navigation
    dropdown
    profile
    settings

    // Profile
    name
    bio
    location
    website
    avatar
    backgroundImage
    updateProfileConfirmButton

    // Actions
    deleteAccountButton
    confirmDeleteAccountButton
    editProfileButton

    constructor(page: Page) {
        this.page = page
        this.dropdown = this.page.locator('[data-cy="auth-nav-dropdown-button"]')
        this.profile = this.page.locator('[data-valuetext="Profile"]')
        this.settings = this.page.locator('[data-valuetext="Settings"]')
        
        this.editProfileButton = this.page.locator('[href="/edit-profile"]')
    
        this.name = this.page.locator('[name="name"]')
        this.bio = this.page.locator('[name="bio"]')
        this.location = this.page.locator('[name="location"]')
        this.website = this.page.locator('[name="website"]')
        this.avatar = this.page.locator('[name="avatar"]')
        this.backgroundImage = this.page.locator('[name="backgroundImage"]')
        this.updateProfileConfirmButton = this.page.getByRole('button', { name: 'Update Profile' })
    }

    async openAccountSettingsDropdown() {
        await this.dropdown.click()
    }

    async navigateToUserProfile() {
        await this.profile.click()
    }

    async navigateToUserSettings() {
        await this.settings.click()
    }

    async clickEditProfileButton() {
        await this.editProfileButton.click()
    }

    async enterNewName(name: string) {
        await this.name.fill(name)
    }

    async enterBio(bio: string) {
        await this.bio.fill(bio)
    }

    async enterLocation(location: string) {
        await this.location.fill(location)
    }

    async enterWebsite(website: string) {
        await this.website.fill(website)
    }

    async enterAvatar(avatarURL) {
        await this.avatar.fill(avatarURL)
    }

    async enterBackgroundImage(imageURL) {
        await this.backgroundImage.fill(imageURL)
    }

    async updateProfile() {
        const name = 'Casey Andthesunshineband'
        const bio = 'example'
        const location = 'here'
        const website = 'https://www.google.com'
        const avatarURL = 'https://httpstatusdogs.com/img/200.jpg'
        const backgroundImage = 'https://httpstatusdogs.com/img/201.jpg'

        await this.enterNewName(name)
        await this.enterBio(bio)
        await this.enterLocation(location)
        await this.enterWebsite(website)
        await this.enterAvatar(avatarURL)
        await this.enterBackgroundImage(backgroundImage)

        await this.updateProfileConfirmButton.click()
    }
}