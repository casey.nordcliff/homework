import { test } from '@playwright/test'

import HomePage from '../components/HomePage'
import SignUpPage from '../components/SignUpPage'
import AccountPage from '../components/AccountPage'

test.describe('TwitterClone', () => {
    test.beforeEach(async ({ page }) => {
        const homePage = new HomePage(page)
        await homePage.navigateToHomePage()
        await homePage.navigateToSignUp()
        await new SignUpPage(page).signUpNewUser()
        await homePage.verifyUserIsLoggedIn()
    })

    test('Page loads correctly', async ({ page }) => {
        await new HomePage(page).verifyPageLoad()
    })

    test('Make tweet', async ({ page }) => {
        const homePage = new HomePage(page)
        const text = `Example tweet - ${Date.now()}`
        await homePage.makeNewTweet(text)
    })

    test('Account settings', async ({ page }) => {
        const accountPage = new AccountPage(page)
        await accountPage.openAccountSettingsDropdown()
        await accountPage.navigateToUserSettings()
        await accountPage.clickEditProfileButton()
        await accountPage.updateProfile()
    })
})