import { defineConfig } from '@playwright/test'

export default defineConfig({
  reporter: 'html',
  use: {
    headless: false,
    viewport: { width: 1920, height: 1080 }
  }
})